/*
 * Requète ajax pour interagir avec le calendrier
 */
var tabDay = ["Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi"];
var tabMonth = ["Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre"];
var week = 0;
var tabData = [];
var color1 = '#a3ce5f';
var color2 = '#ea6464';
var idUser = '';
var allPeople = true;
var idPopUp = 0;
var tabPopUp = new Object();
var currentDate;

jQuery(document).ready(function(){
    // Initialisation
    idUser = jQuery('#user').val();
    currentDate = new Date();
    currentDate.setHours( 0,0,0,0 );
    requestWeek();
    jQuery('#addNew').on('click',showForm);
    jQuery('#cancelForm').on('click',hideForm);
    jQuery('#validForm').on('click',submitForm);
    jQuery('.btnNext').on('click',changeWeek);
    jQuery('.btnBack').on('click',changeWeek);
    jQuery('#calendarMode').on('click',changeMode);
    jQuery('#date').on('change',eraseErr);
    jQuery('#startTime').on('change',eraseErr);
    jQuery('#endTime').on('change',eraseErr);
    jQuery('#modal-button').on('click',closeModal);
    jQuery('.supprTime').on('click',supprTime);
    jQuery('.modifTime').on('click',modifTime);
    jQuery('#modifModal-button').on('click',submitModif);
    jQuery('.btnPeople').on('click',showPeople);
    jQuery('#eventList').on('click',function(){
        jQuery('#eventModal').show();
    });
    jQuery('#eventModal-button').on('click',function(){
        jQuery('#eventModal').hide();
    });
    jQuery('#monthModal-button').on('click',function(){
        jQuery('#monthModal').hide();
    });
    jQuery(document).on('mousemove', function(e){
        var y = e.pageY - jQuery(window).scrollTop() + 25;
        jQuery('#bookInfo').css({left:e.pageX,top:y});
    });
    jQuery('#goWeek').on('click',goWeek);
    jQuery('#seeMonth').on('click',seeMonth);
    jQuery('.monthDay').on('click',monthDay);
});

function changeWeek(){
    // Changer la semaine du calandrier
    if(jQuery(this).hasClass('btnNext')){
        week++;
    }else{
        week--;
    }    
    var date = new Date(jQuery('#firstDay').val());
    date.setDate(date.getDate() + 7*week);
    for (let i = 0; i < 7; i++){
        jQuery('#'+tabDay[date.getDay()]).html(tabDay[date.getDay()]+' '+date.getDate()+' '+tabMonth[date.getMonth()]+' '+date.getFullYear());
        
        jQuery('#'+tabDay[date.getDay()]).next('.btnPeople').attr('data-date',date.getFullYear()+'-'+("0"+(date.getMonth()+1)).slice(-2)+'-'+("0"+date.getDate()).slice(-2));
        date.setDate(date.getDate() +1);
    }
    jQuery('.btnPeople').children('span').html('0');
    jQuery('.people').html('');
    jQuery('.people').hide();
    jQuery(".calendarCase").css('background-color','white');
    jQuery(".calendarCase").css('border-left','none');
    jQuery(".calendarCase").css('border-right','none');
    jQuery(".calendarCase").unbind();
    jQuery(".popUp").hide();
    jQuery(".ajaxLoad").show();
    jQuery.ajax({
        url: ajaxurl,
        data: { 'action': 'dataCalendar','week': week },
        success: drawData,
        error: function(){console.log('erreur Ajax');}
    });
}
    
function requestWeek(){
    // Requete pour obtenir les données d'une semaine
    jQuery.ajax({
        url: ajaxurl,
        data: { 'action': 'dataCalendar','week': week },
        success:drawData,
        error: function(){console.log('erreur Ajax');}
    });
}

function drawData(data){
    // Dessine les données reçus
    jQuery('.calendarCase').removeClass('full');
    data[0].forEach(function(evt){  
        var date = new Date(evt.date);
        var color;
        if(evt.user_id === idUser){
            color = color1;
        }else{
            color = color2;
        }
        var splitStart = evt.start_time.split(':');
        var splitEnd = evt.end_time.split(':');
        var start = Number(splitStart[0]);
        var end = Number(splitEnd[0]);
        if(splitStart[1] === '30'){
            start += '-30';
        }
        if(splitEnd[1] === '30'){
            end += '-30';
        }
        var idStart = evt.event_type + '_' + tabDay[date.getDay()] + '_' + start;
        var idEnd = evt.event_type + '_' + tabDay[date.getDay()] + '_' + end;
        splitStart[0] = Number(splitStart[0]);
        jQuery('#'+idStart).css('border-left','1px solid black'); 
        addPopUp(evt.start_time,evt.end_time,evt.user_name,idStart);
        while(idStart !== idEnd){
            var lastId = idStart;
            jQuery('#'+idStart).on( "mouseenter mouseleave",showPopUp);
            tabPopUp[idStart] = idPopUp;
            if(jQuery('#'+idStart).css('background-color') !== color1){
                jQuery('#'+idStart).css('background-color',color);
                
                
                
                jQuery('#'+idStart).addClass('full');
                
            }
            if(splitStart[1] === '00'){
                splitStart[1] = '30';
                idStart = idStart+'-30';
            }else{
                splitStart[1] = '00';
                splitStart[0]++;
                if(splitStart[0] > 10){
                    idStart = idStart.substr(0,idStart.length - 5)+splitStart[0];
                }else{
                    idStart = idStart.substr(0,idStart.length - 4)+splitStart[0];
                }                
            }
            if(idStart === idEnd){
                jQuery('#'+lastId).css('border-right','1px solid black');
            }
        }
    });
    clickableCase();
    addPeopleByDay(data[1]);
    addEvtByDay(data[2]);
    jQuery(".ajaxLoad").hide();
}

function clickableCase(){
    // Rendre les case libre du calendrier clickable pour réserver un pont
    jQuery('.clickable').unbind();
    jQuery('.calendarCase').removeClass('clickable');
    jQuery('.calendarCase').each(function(){
        if(!jQuery(this).hasClass('full')){
            jQuery(this).addClass('clickable');
        }
    });
    jQuery('.clickable').on('click',function(){
        jQuery('#date').val(jQuery(this).parents('table').prev().prev().attr('data-date'));
        var splitId = jQuery(this).attr('id').split('_');
        var pont = splitId[0].split('-');
        if(pont[0] === "Parallélisme"){
            jQuery('#event_type').val('6');
        }else{
            jQuery('#event_type').val(pont[1]);
        }
        var splitTime = splitId[2].split('-');
        var time = splitTime[0];
        if(splitTime[1]){
            var endTime = (parseInt(time)+1)+':00';
            time += ':30';
        }else{
            var endTime = time+':30';
            time += ':00';
        }
        jQuery('#startTime').val(time);
        jQuery('#endTime').val(endTime);
        showForm();
    });
    jQuery('.clickable').mouseenter(function(){
        var splitId = jQuery(this).attr('id').split('_');
        var splitTime = splitId[2].split('-');
        var time = splitTime[0];
        if(splitTime[1]){
            time += ':30';
        }else{
            time += ':00';
        }
        jQuery(this).css('background-color','#7ceae8');
        jQuery('#bookInfo').children('p').children('span').html(time);
        jQuery('#bookInfo').show();
    });
    jQuery('.clickable').mouseleave(function(){
        jQuery(this).css('background-color','white');
        jQuery('#bookInfo').hide();
    });
}



function addPopUp(start,end,name,idStart){
    // Créer une info bulle pour une page horaire
    idPopUp++;
    jQuery('#'+idStart).html('<span class="popUp" id="popUp-'+idPopUp+'">'+name+' '+start.slice(0,5)+'-'+end.slice(0,5)+'</span>'); 
}

function addEvtByDay(data){
    // Afficher les evt sur le calendrier
    jQuery('.evtDay').html('');
    for (var day in data) {
       data[day].forEach(function(evt){
          jQuery('#'+day).append('<h5>'+evt['title']+' <span>De '+evt['start_time']+' à '+evt['end_time']+'</span></h5>');    
          jQuery('#'+day).append('<p>'+evt['description']+'</p>');  
       });
    }
}

function showPopUp(){
    // Affiché ou caché les infos d'une réservation
    var id = tabPopUp[jQuery(this).attr('id')];
    if(jQuery("#popUp-"+id).css('display') === 'none'){
        jQuery("#popUp-"+id).show();
    }else{
        jQuery("#popUp-"+id).hide();
    }
}

function addPeopleByDay(data){
    // Ajouter les nom des personne présente chaque jour
    for(var date in data){
        var nbr = 0;
        var text = "";
        for(var id in data[date]){
            nbr ++;
            text += "<p>"+data[date][id]['name']+": </p>";
            text += "<ul>";
            for (var time in data[date][id]['time']){
                text += "<li>De "+data[date][id]['time'][time][0].slice(0,5)+" à "+data[date][id]['time'][time][1].slice(0,5)+" "+data[date][id]['time'][time][2]+"</li>";
            }
            text += "</ul><br>";
        }
        text = text.substr(0,text.length-2);
        jQuery("button[data-date*="+date+"]").next('.people').html(text);
        jQuery("button[data-date*="+date+"]").children('span').html(nbr);
    }  
}

function showPeople(){
    // Afficher ou cacher les personne présente du jour
    var div = jQuery(this).next('.people');
    if(div.css('display') === 'none'){
        div.show();
    }else{
        div.hide();
    }
}

function showForm(){
    // Affiché formulaire
    jQuery('#addNew').hide();
    jQuery('.calendarMenu').hide();
    if(allPeople){
        jQuery('.calendarMain').hide();
    }else{
        jQuery('.myCalendar').hide();
    }
    eraseErr();
    jQuery('#form').show();
}

function hideForm(){
    // Caché le formulaire
    jQuery('#addNew').show();
    jQuery('.calendarMenu').show();
    if(allPeople){
        jQuery('.calendarMain').show();
    }else{
        jQuery('.myCalendar').show();
    }
    jQuery('#form').hide();
}

function submitForm(){
    // Envoyer le formulaire si il est rempli correctement
    var user = jQuery('#user').val();
    var event_type = jQuery('#event_type').val();
    var start_time = jQuery('#startTime').val();
    var end_time = jQuery('#endTime').val();
    var date = jQuery('#date').val();
    var error = false;
    if(date === ""){
        jQuery('#errDate').html("Vous n'avez pas renseigné la date");
        error = true;
    }
    if(start_time === end_time){
        jQuery('#errStart').html("L'heure d'arrivée et de départ sont identiques");
        error = true;
    }
    if(!error){
        var splitStart = start_time.split(':');
        var splitEnd = end_time.split(':');
        if(Number(splitStart[0]) > Number(splitEnd[0])){
            error = true;
        }else if(splitStart[0] === splitEnd[0] && splitEnd[1] === '00'){
            error = true;
        }
        if(error){
            jQuery('#errStart').html("L'heure d'arrivée est ultérieur à celle de départ");
        }else{
            sendForm(user,event_type,start_time,end_time,date);
        }
    }
}
function eraseErr(){
    // efface les message d'erreur du formulaire
    jQuery('.errorForm').html("");
}

function sendForm(user,event_type,start_time,end_time,date){
    // Envoi des donnée du formulaire
    jQuery(".ajaxLoad").show();
   jQuery.ajax({
        url: ajaxurl,
        data: { 'action': 'insertCalendar','user': user,'event_type': event_type,'start_time': start_time,'end_time': end_time,'date': date},
        success:insertSuccess,
        error: function(){console.log('erreur Ajax');}
    });
}

function insertSuccess(data){
    // Informer l'utilisateur sur la réussite de l'insertion
    if(data.success){
        jQuery('#modal-content').children('p').html('Vos information ont bien été ajouté au calandrier');
        jQuery('#modal').show();
        hideForm();
        requestWeek();
        majMyCalendar();
    }else{
        jQuery('#errorForm').html(data.data);
    }
    jQuery(".ajaxLoad").hide();
}

function majMyCalendar(){
    // Mise à jour du calandrier personnel
    jQuery.ajax({
        url: ajaxurl,
        data: { 'action': 'majMyCalendar'},
        success: function(data){
            jQuery('#myCalendar-content').html(data);
            jQuery('.supprTime').on('click',supprTime);
            jQuery('.modifTime').on('click',modifTime);
        },
        error: function(){console.log('erreur Ajax');}
    });
}

function supprTime(){
    // Vérifie que l'utilisateur peut suprimer un horaire si oui envoyer la requete
    if(checkTime(jQuery(this).attr('data-date'),jQuery(this).attr('data-start'))){
        var user = jQuery('#user').val(); 
        var id = jQuery(this).attr('data-id');
        jQuery(".ajaxLoad").show();
        jQuery.ajax({
            url: ajaxurl,
            data: { 'action': 'supprCalendar','user': user,'id': id},
            success:function(data){
                if(data === '1'){
                    majMyCalendar();
                    jQuery(".calendarCase").css('background-color','white');
                    jQuery(".calendarCase").css('border-left','none');
                    jQuery(".calendarCase").css('border-right','none');
                    requestWeek();
                    jQuery('#modal-content').children('p').html('La supression a été réalisée');
                }else{
                    jQuery('#modal-content').children('p').html('La supression a échoué');
                }
                jQuery(".ajaxLoad").hide();
                jQuery('#modal').show();
            },
            error: function(){
                jQuery('#modal-content').children('p').html('La supression a échoué');
                jQuery(".ajaxLoad").hide();
                jQuery('#modal').show();
            }
        });
    }else{
        jQuery('#modal-content').children('p').html('Vous ne pouvez pas suprimer cette horaire car il a débuté il y a plus de 30 minutes');
        jQuery('#modal').show();
    }
}
function modifTime(){
    // Vérifier que l'utilisateur peut modifier un horraire, si oui lui affiché un formulaire
    var date = jQuery(this).attr('data-date');
    var start = jQuery(this).attr('data-start');
    var end = jQuery(this).attr('data-end');
    var id = jQuery(this).attr('data-id');
    if(checkTime(date,end)){
        if(!checkTime(date,start)){
            jQuery('#modifStart').attr('disabled','disabled');
        }else{
            jQuery('#modifStart').attr('disabled',false);
        }
        var splitDate = date.split('-');
        var valStart;
        var valEnd;        
        if(start.slice(0,1) === "0"){valStart = start.slice(1,5);}else{valStart = start.slice(0,5);}
        if(end.slice(0,1) === "0"){valEnd = end.slice(1,5);}else{valEnd = end.slice(0,5);} 
        jQuery('#modifDate').html(splitDate[2]+"/"+splitDate[1]+"/"+splitDate[0]);
        jQuery('#modifStart').val(valStart);
        jQuery('#modifEnd').val(valEnd);
        jQuery('#modifModal-button').attr('data-id',id);
        jQuery('#errModif').html('');
        jQuery('#modifModal').show();
    }else{
        jQuery('#modal-content').children('p').html('Vous ne pouvez pas suprimer cette horaire car il est terminé depuis plus de 30 minutes');
        jQuery('#modal').show();
    }   
}

function submitModif(){
    // Envoyer le formulaire de modification si il passe les verifications
    var id = jQuery(this).attr('data-id');
    var start_time = jQuery('#modifStart').val();
    var end_time = jQuery('#modifEnd').val();
    var user = jQuery('#user').val();
    var error = false;
    if(start_time === end_time){
        jQuery('#errModif').html("L'heure d'arrivée et de départ sont identiques");
        error = true;
    }
    if(!error){
        var splitStart = start_time.split(':');
        var splitEnd = end_time.split(':');
        if(Number(splitStart[0]) > Number(splitEnd[0])){
            error = true;
        }else if(splitStart[0] === splitEnd[0] && splitEnd[1] === '00'){
            error = true;
        }
        if(error){
            jQuery('#errModif').html("L'heure d'arrivée est ultérieur à celle de départ");
        }else{
            jQuery(".ajaxLoad").show();
            jQuery.ajax({
                url: ajaxurl,
                data: { 'action': 'modifCalendar','id': id,'start_time': start_time, 'end_time': end_time,'user': user},
                success:function(data){
                    if(data === '1'){
                        majMyCalendar();
                        jQuery(".calendarCase").css('background-color','white');
                        requestWeek();
                        jQuery('#modal-content').children('p').html('La modification a été réalisée');
                    }else{
                        jQuery('#modal-content').children('p').html('La modification a échoué');
                    }
                    jQuery(".ajaxLoad").hide();
                    jQuery('#modal').show();
                    jQuery('#modifModal').hide();
                },
                error: function(){
                    jQuery(".ajaxLoad").hide();
                    jQuery('#modal-content').children('p').html('La modification a échoué');
                    jQuery('#modal').show();
                    jQuery('#modifModal').hide();
                }
            });
        }
    }
}

function checkTime(date,time){
    // Vérifier qu' un horraire n'est pas dépassé de 30 minutes
    var currentDate = new Date();
    var splitDate = date.split('-');
    var splitTime = time.split(':');
    var date = new Date(splitDate[0],splitDate[1]-1,splitDate[2],splitTime[0],splitTime[1]);
    var diff = date - currentDate;
    if(diff > -1800000){
        return true;
    }else{
        return false;
    }
}

function changeMode(){
    // Changer l'affichage entre le calandrier général et les horaire de la personne connecté
    if(allPeople){
        jQuery('#calendarMode').html('<i class="fas fa-arrow-left"> Calendrier');
        jQuery('.myCalendar').show();
        jQuery('.calendarMain').hide();
        jQuery('.btnBack').hide();
        jQuery('.btnNext').hide();
        allPeople = false;
    }else{
        jQuery('#calendarMode').html('<i class="fas fa-eye"></i> Mon emploi du temps');
        jQuery('.myCalendar').hide();
        jQuery('.calendarMain').show();
        jQuery('.btnBack').show();
        jQuery('.btnNext').show();
        allPeople = true;
    }
}

function closeModal(){
    // Fermer le modal
    jQuery('#modal').hide();
}

function goWeek(){
    // Calculer le nombres de semaines de différence entre le calandrier et la date saisie
    if(jQuery(this).prev().val()){
        var date = new Date(jQuery(this).prev().val());
        date.setHours( 0,0,0,0 );
        if(date.getDay() === 0){
            var newMon = date - 6*86400000;
        }else{
            var newMon = date - (date.getDay()-1)*86400000;
        }
        if(currentDate.getDay() === 0){
            var mon = currentDate - 6*86400000;
        }else{
            var mon = currentDate - (currentDate.getDay()-1)*86400000;
        }
        var diff = parseInt((newMon - mon)/(86400000*7));
        if(week !== diff){
            week = diff+1;
            changeWeek();
        }
    }
}

function seeMonth(){
    // Voir les adhérents présents sur les 30 prochain jours
    jQuery(".ajaxLoad").show();
    jQuery.ajax({
        url: ajaxurl,
        data: { 'action': 'seeMonth'},
        success:function(data){
            data.forEach(function(day,key){
                console.log(day);
                jQuery('#day-'+key).children('h4').html(day.date);
                jQuery('#day-'+key).attr('data-date',day.attrDate);                
                var nameList = "";
                for (const pres of Object.entries(day.pres)) {nameList += '<li>'+pres[1].name+' ('+pres[1].time+')</li>';}
                if(nameList !== ""){
                    jQuery('#day-'+key).children('ul').removeClass('noPeople');
                    jQuery('#day-'+key).children('ul').html(nameList.substr(0,nameList.length -2));
                }else{
                    jQuery('#day-'+key).children('ul').addClass('noPeople');
                    jQuery('#day-'+key).children('ul').html('<li>aucun adhérent</li>');
                }
            });
            jQuery('#monthModal').show();
            jQuery(".ajaxLoad").hide();
        },
        error: function(){
            jQuery('#modal-content').children('p').html('Une erreur est survenue');
            jQuery('#modal').show();
            jQuery('.ajaxLoad').hide();
        }
    });
}

function monthDay(){
    // Aller au formilaire de réservation depuis le calendrier du mois
    jQuery('#date').val(jQuery(this).attr('data-date'));
    jQuery('#event_type').val('4');
    showForm();
    jQuery('#monthModal').hide();
}