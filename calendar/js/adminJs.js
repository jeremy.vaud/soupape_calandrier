/* 
 * Srcript spécifique au mode administrateur
 */

var mode = 'addAdmin';

jQuery(document).ready(function(){    
    initAdmin();
    jQuery('#openAdmin').on('click',openAdmin);
    jQuery('#addAdmin, #modifAdmin, #newEvt,#download').on('click',adminMode);
    jQuery('#submitAdd').on('click',submitAdd);
    jQuery('#submitNewEvt').on('click',submitNewEvt);
    jQuery('#modifAdminSee').on('click',listModif);
    jQuery('#csvDownload').on('click',submitDownload);
    jQuery('#csvStart, #csvEnd').on('change',function(){
        jQuery('#errCsv').hide();
    });
    jQuery(".validation").on("click",validation);
    jQuery(".annulation").on("click",annulation);
    jQuery("#cancelModif").on('click',function(){
        jQuery('.modifAdminForm').hide();
    });
    jQuery("#submitModif").on('click',submitModifAdmin);
});

function initAdmin(){
    // initialisation de la div admin
    jQuery('#selectUser').select2();
    jQuery('.admin,.modifAdmin,.newEvt,.download').hide();
}

function openAdmin(){
    // Ouvrir la div admin
    if(jQuery('.admin').css('display') === 'none'){
        jQuery('.admin').css('display','block');
        jQuery('#openAdmin').children('i').removeClass('fa-caret-up');
        jQuery('#openAdmin').children('i').addClass('fa-caret-down');
    }else{
        jQuery('.admin').css('display','none');
        jQuery('#openAdmin').children('i').removeClass('fa-caret-down');
        jQuery('#openAdmin').children('i').addClass('fa-caret-up');
    }
}

function adminMode(){
    // Changer de mode(ajout,modif,...)
    jQuery('.btnAdmin').removeClass('actifAdmin');
    jQuery(this).addClass('actifAdmin');
    mode = jQuery(this).attr('id');
    jQuery('.addAdmin,.modifAdmin,.newEvt,.download,.selectUser').hide();
    jQuery('#errAdd,#listModif').html('');
    if(mode === 'addAdmin'){
        jQuery('.selectUser,.addAdmin').show();
    }else if(mode === 'modifAdmin'){
        jQuery('.modifAdminForm').hide();
        jQuery('.selectUser,.modifAdmin').show();
    }else if(mode === 'newEvt'){
        jQuery('.newEvt').show();
    }else if(mode === 'download'){
        jQuery('.download').show();
    }
}

function submitAdd(){
    // Vérifer les donnée pour l'ajout et envoyer une requete ajax
    var user = jQuery('#selectUser').val();
    var event_type = jQuery('#adminAddType').val();
    var date = jQuery('#adminAddDate').val();
    var start_time = jQuery('#adminAddStart').val();
    var end_time = jQuery('#adminAddEnd').val();
    var error = false;
    if(start_time === end_time){
        jQuery('#errAdd').html("L'heure d'arrivée et de départ sont identiques");
        error = true;
    }
    if(date === ""){
        jQuery('#errAdd').html("Vous n'avez pas renseigner la date");
        error = true;
    }
    if(!error){
        var splitStart = start_time.split(':');
        var splitEnd = end_time.split(':');
        if(Number(splitStart[0]) > Number(splitEnd[0])){
            error = true;
        }else if(splitStart[0] === splitEnd[0] && splitEnd[1] === '00'){
            error = true;
        }
        if(error){
            jQuery('#errAdd').html("L'heure d'arrivée est ultérieur à celle de départ");
        }else{
            jQuery(".ajaxLoad").show();
            jQuery.ajax({
                url: ajaxurl,
                data: { 'action': 'insertCalendar','user': user,'event_type': event_type,'start_time': start_time,'end_time': end_time,'date': date,'admin':true},
                success:insertAdmin,
                error: function(){
                    console.log('erreur Ajax');
                    jQuery(".ajaxLoad").hide();
                }
            });
        }
    }
}

function insertAdmin(data){
    // Informer l'administrateur sur la réussite de l'insertion
    if(data.success){
        jQuery('#modal-content').children('p').html('Vos information ont bien été ajouté au calandrier');
        jQuery('#modal').show();
        jQuery('#errAdd').html('');
        requestWeek();
        majMyCalendar();
    }else{
        jQuery('#errAdd').html(data.data);
    }
    jQuery(".ajaxLoad").hide();
}

function submitNewEvt(){
    // Requete pour créer un nouvel évenement
    var title = jQuery('#newEvtTitle').val();
    var date = jQuery('#newEvtDate').val();
    var time_start = jQuery('#newEvtStart').val();
    var time_end = jQuery('#newEvtEnd').val();
    var description = jQuery('#description').val();
    if(title === "" || date === "" || time_start === "" || time_end === "" || description === ""){
        jQuery("#errNewEvt").html("Veuillez remplir tous les champs");
    }else{
        jQuery(".ajaxLoad").show();
        jQuery.ajax({
            url: ajaxurl,
            data:{'action': 'insertEvt','title': title,'start_time': time_start,'end_time': time_end,'date': date,'description': description},
            success: insertEvt,
            error: function(){
                console.log('erreur Ajax');
                jQuery(".ajaxLoad").hide();
            }
        });
    }
}

function insertEvt(data){
    // Confirmer l'ajout d'un évenement
    if(data[0]){
        jQuery('#modal-content').children('p').html("L'évenement été ajouté au calandrier");       
        jQuery('#eventModal-content').html(data[1]);
        jQuery('#eventModal-content').append('<button id="eventModal-button">Fermer</button>');
        jQuery('#eventModal-button').on('click',function(){
        jQuery('#eventModal').hide();
    });
    }else{
        jQuery('#Modal-content').html("Une erreur c'est prouduite, veuilleez réessayer");
    }
    jQuery(".ajaxLoad").hide();
    jQuery('#modal').show();
}

function listModif(){
    // Requete pour obtenir les crénaux d'un adhérent à une date donnée
    jQuery('#modifAdminSee').unbind();
    var user = jQuery('#selectUser').val();
    var date = jQuery('#modifAdminDate').val();
    var splitDate = date.split('-');
    jQuery('#listModif').html("<h5>"+jQuery('#selectUser option:selected').text()+" le "+splitDate[2]+"/"+splitDate[1]+"/"+splitDate[0]+"</h5>");
    jQuery(".ajaxLoad").show();
    jQuery.ajax({
            url: ajaxurl,
            data:{'action': 'userDate','user': user,'date': date},
            success: showListModif,
            error: function(){
                console.log('erreur Ajax');
                jQuery(".ajaxLoad").hide();
            }
        });
}

function showListModif(data){
    // Afficher liste des evt de l'adhérent
    jQuery('.modifAdminForm').hide();
    if(data.length < 1){
        jQuery('#listModif').append('<p>Aucun résultat trouvé</p>');
    }else{       
        var currentDate = new Date();
        var date = new Date(data[0].date);
        var modifPont = false;
        if(currentDate > date){
            modifPont = true;
        }
        data.forEach(function(evt){
            if(evt.start_time.substr(0,1) === '0'){
                var startTime = evt.start_time.substr(1,4);
            }else{
                var startTime = evt.start_time.substr(0,5);
            }
            if(evt.end_time.substr(0,1) === '0'){
                var endTime = evt.end_time.substr(1,4);
            }else{
                var endTime = evt.end_time.substr(0,5);
            }
            var html = "<div>";
            if(evt.modif_id === null){
                if(!modifPont && (evt.event_type === "pont-1" || evt.event_type === "pont-2")){
                    html += "<button class='btnModifAdmin' data-id='"+evt.id+"' disabled='disabled'><i class='fas fa-pencil-alt'></i> Modifier</button>";
                }else{
                    html += "<button class='btnModifAdmin' data-id='"+evt.id+"' data-start='"+startTime+"' data-end='"+endTime+"'><i class='fas fa-pencil-alt'> Modifier</i></button>";
                }
                html += "<button class='btnSupprAdmin' data-id='"+evt.id+"'><i class='fas fa-trash-alt'></i> Suprimer</button>";
            }else{ 
                html  += "<button class='btnModifAdmin' data-id='"+evt.id+"' disabled='disabled'><i class='fas fa-pencil-alt'></i> Modifier</button><button class='btnSupprAdmin' data-id='"+evt.id+"' disabled='disabled'><i class='fas fa-trash-alt'></i> Suprimer</button>";
            }
            html += "<span> De "+startTime+" à "+endTime+" : "+evt.event_type+"</div>";
            jQuery('#listModif').append(html);
        });        
    }
    jQuery(".ajaxLoad").hide();
    jQuery('#modifAdminSee').on('click',listModif);
    jQuery('.btnModifAdmin').on('click',modifAdmin);
    jQuery('.btnSupprAdmin').on('click',supprAdmin);
}

function modifAdmin(){
    // Afficher le formulaire pour modifier les horaires
    jQuery('#idModif').val(jQuery(this).attr('data-id'));
    jQuery('#modifStart').val(jQuery(this).attr('data-start'));
    jQuery('#modifEnd').val(jQuery(this).attr('data-end'));
    jQuery('.modifAdminForm').show();
}

function submitModifAdmin(){
    // Envoyer le formulaire de modification d'horraire
    var id = jQuery('#idModif').val();
    var start = jQuery('#modifStart').val();
    var end = jQuery('#modifEnd').val();
    jQuery(".ajaxLoad").show();
    jQuery.ajax({
        url: ajaxurl,
        data:{'action': 'modifAdmin','admin':idUser,'idLine': id,'start-time': start,'end-time': end},
        success: successModif,
        error: function(){
            console.log('erreur Ajax');
            jQuery(".ajaxLoad").hide();
        }
    });
}

function supprAdmin(){
    // Requete pour suprimer des horaire
    var idEvt = jQuery(this).attr('data-id');
    jQuery(".ajaxLoad").show();
    jQuery.ajax({
        url: ajaxurl,
        data:{'action': 'supprAdmin','admin':idUser,'idLine':idEvt},
        success: successModif,
        error: function(){
            console.log('erreur Ajax');
            jQuery(".ajaxLoad").hide();
        }
    });  
}

function successModif(data){
    // Confirmer à l'admin que sa demande ou sa validation de modification à été enregistré
    if(!data['error']){
        jQuery('#validation').html(data['list'].html);
        jQuery('#nbrValidation').html(data['list'].nbr);
        jQuery(".validation").on("click",validation);
        jQuery(".annulation").on("click",annulation);
    }
    jQuery('#modal-content').children('p').html(data['msg']);
    jQuery('#modal').show();
    jQuery(".ajaxLoad").hide();
}

function validation(){
    // Valider la modification d'un autre administrateur
    var idModif = jQuery(this).attr('data-modif');
    var idEvt = jQuery(this).attr('data-evt');
    jQuery(".ajaxLoad").show();
    jQuery.ajax({
        url: ajaxurl,
        data:{'action': 'validModif','admin':idUser,'idEvt':idEvt,'idModif':idModif},
        success: successModif,
        error: function(){
            console.log('erreur Ajax');
            jQuery(".ajaxLoad").hide();
        }
    });
}

function annulation(){
    // Annuler une modifiction d' administrateur
    var id = jQuery(this).attr('data-id');
    jQuery(".ajaxLoad").show();
    jQuery.ajax({
        url: ajaxurl,
        data:{'action': 'deleteModif','id':id},
        success: successModif,
        error: function(){
            console.log('erreur Ajax');
            jQuery(".ajaxLoad").hide();
        }
    });
}

function submitDownload(){
    // Requete ajax pour le telechargement d'une partie du calendrier
    var csvStart = jQuery('#csvStart').val();
    var csvEnd = jQuery('#csvEnd').val();
    var start = new Date(csvStart);
    var end = new Date(csvEnd);
    if (start > end){
        jQuery('#errCsv').show();
    }else{
        jQuery(".ajaxLoad").show();
        jQuery.ajax({
            url: ajaxurl,
            data:{'action': 'csvDownload','csvStart':csvStart,'csvEnd':csvEnd},
            success: download,
            error: function(){
                console.log('erreur Ajax');
                jQuery(".ajaxLoad").hide();
            }
        });
    }
}

/////////////////////////////////////////////////////////////////////////////////
function download(data){
    // Telechargement d'une partie du calendrier au format csv
    // Données csv à confirmer
    console.log(data);
    jQuery(".ajaxLoad").hide();
}
/////////////////////////////////////////////////////////////////////////////////