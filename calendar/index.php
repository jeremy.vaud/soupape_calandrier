<?php
/* 
 * Index calendrier de la soupape
 */

// Verif erreur
ini_set('display_errors',1);
error_reporting(E_ALL);

// Configuration du calandrier
include 'config/calendarConfig.php';

// Inclures les fonctions
include 'lib/inputForm.php';
include 'lib/ajaxEnqueue.php';

// Ajout des fonctionnalité d'aministration
$adminTool = "";
if(current_user_can('administrator')){
    include 'lib/adminFunction.php';
    wp_enqueue_style('select2', "https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css");
    wp_enqueue_script('select2', "https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js");
    admin_enqueue();
    $adminTool .= adminTemplate($timeStart,$timeEnd);
}else{
    include 'lib/createCalendar.php';
}

// Récupération de l'id de l'utilisateur
$idUser = get_current_user_id();

// Création du calendrier général
$calendar = generateCalendar($timeStart, $timeEnd,$tabLine);

// Création de la liste des évenement à venir
$eventList = eventList();

// Création des select des formulaires
$event_type = selectEvent('event_type');
$selectStart = selectTime('startTime', $timeStart, $timeEnd);
$selectEnd = selectTime('endTime', $timeStart, $timeEnd);
$modifStart = selectTime('modifStart',$timeStart, $timeEnd);
$modifEnd = selectTime('modifEnd',$timeStart, $timeEnd);

// Création du calandrier de l'utilisateur
$myCalendar = generateMyCalendar($idUser);

// Création du calendrier du mois
$monthCalendar = generateMonth();

// Template du calandrier
include 'template/calendar.php';

// Ajout du css du calandrier
wp_enqueue_style('calendarCss', '/calendar/css/calendarStyle.css');

// Css font-Awesome
wp_enqueue_style('fontAwesome', "https://use.fontawesome.com/releases/v5.7.1/css/all.css");

// Ajout javascript
ajax_enqueue();

