<!-- Template du calendrier -->
<?=$adminTool?>
<div class="calendar">
    <input type="hidden" id="user" value="<?=$idUser?>"/>
    <div class="userMenu">
        <button id='addNew'><i class="fas fa-plus"></i> Je viens à l'atelier</button>
        <button id="seeMonth"><i class="fas fa-eye"></i> Présences du mois</button>
        <button id="eventList"><i class="far fa-calendar-alt"></i> Evenements à venir</button>
    </div>
    <div class="calendarMenu">
        <button class="btnBack"><i class="fas fa-arrow-left"></i></button>
        <button id="calendarMode"><i class="fas fa-eye"></i> Mon emploi du temps</button>
        <button class="btnNext"><i class="fas fa-arrow-right"></i></button>       
    </div>  
    <div class="calendarMain">
        <div class="goWeek">
            <input type="date"/>
            <button type="date" id="goWeek">Go</button>
        </div>
        <?=$calendar?>
    </div>
    <form id='form' onsubmit="return false">
        <h5>Je viens à l'atelier</h5>
        <label for='event_type'>Je viens pour: </label>
        <?=$event_type?>
        <label for='date'>Date: <span class='errorForm' id='errDate'></span></label>
        <input id='date' type="date"/>
        <label for='startTime'>Heure d'arrivée: <span class='errorForm' id='errStart'></span></label>
        <?=$selectStart?>
        <label for='endTime'>Heure de départ: </label>
        <?=$selectEnd?>      
        <input id='cancelForm' type="submit" value="Annuler"/>
        <input id='validForm' type="submit" value="Valider"/>
        <span id='errorForm' class="errorForm"></span>
    </form>
    <div class="myCalendar">
        <h3>Mon emploi du temps</h3>
        <div id='myCalendar-content'>
            <?=$myCalendar?>
        </div>
    </div>
</div>
<div id="modal">
    <div id="modal-content">
        <p></p>
        <button id="modal-button">ok</button>
    </div>
</div>
<div id="modifModal">
    <div id="modifModal-content">
        <h5>Modifification des horaires du <span id="modifDate"></span></h5>
        <p id="errModif"></p>
        <label for="modifStart">Heure d'arrivée: </label>
        <?=$modifStart?>
        <label for='modifEnd'>Heure de départ: </label>
        <?=$modifEnd?>
        <button id="modifModal-button" data-id="0">Valider</button>
    </div>
</div>
<div id="eventModal">
    <div id="eventModal-content">
        <?=$eventList?>
        <button id="eventModal-button">Fermer</button>
    </div>
</div>
<div id="monthModal">
    <div id="monthModal-content">
        <h3>Présences sur un mois</h3>
        <?=$monthCalendar?>
        <button id="monthModal-button">Fermer</button>
    </div>
</div>
<div class="ajaxLoad">
    <img width="128" alt="YouTube loading symbol 3 (transparent)" src="https://upload.wikimedia.org/wikipedia/commons/a/ad/YouTube_loading_symbol_3_%28transparent%29.gif">
</div>
<div id="bookInfo">
    <p>Réserver le pont pour <span></span></p>
</div>