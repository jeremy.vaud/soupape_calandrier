<!-- Outils administrateur-->
<button class="btnAdmin" id="openAdmin"><i class="fas fa-caret-up"></i> Admin</button><span>Validations en attente: <span id="nbrValidation"><?=$nbrValidation?></span></span>
<div class="admin">
    <div id="validation">
        <?=$listValidation?>
    </div>
    <h5>Outils d'aministration</h5>
    <button class="btnAdmin actifAdmin" id="addAdmin">Ajouter</button>
    <button class="btnAdmin" id="modifAdmin">Modifier</button>
    <button class="btnAdmin" id="newEvt">Créer un évenement</button>
    <button class="btnAdmin" id="download">Télécharger</button>
    <div class="selectUser">
        <label for="selectUser">Nom:</label>
        <select id="selectUser">
            <?=$listUser?>
        </select>
    </div>
    <div class="addAdmin">
        <label for='adminAddType'>L'adérent viens pour: </label>
        <?=$admin_type?>
        <label for="adminAddDate">Date: </label>
        <input type="date" id="adminAddDate"/>
        <label for='adminAddStart'>Heure d'arrivée: </label>
        <?=$selectStartAdmin?>
        <label for='adminAddEnd'>Heure de départ: </label>
        <?=$selectEndAdmin?> 
        <button id="submitAdd">Valider</button>
        <span id="errAdd"></span>
    </div>
    <div class="modifAdmin">
        <input type="date" id="modifAdminDate"/>
        <button id="modifAdminSee">Rechercher</button>
        <div id="listModif"></div>
        <div class="modifAdminForm">
            <h5>Modification des horaires</h5>
            <input type="hidden" id="idModif"/>
            <label for='modifStart'>Heure d'arrivée: </label>
            <?=$modifStartAdmin?>
            <label for='modifEnd'>Heure de départ: </label>
            <?=$modifEndAdmin?>
            <button id="cancelModif">Annuler</button>
            <button id="submitModif">Valider</button>
        </div>
    </div>
    <div class="newEvt">
        <label for="newEvtTitle">Titre:</label>
        <input type="text" id="newEvtTitle"/>
        <label for="newEvtDate">Date:</label>
        <input type="date" id="newEvtDate"/>
        <label for="newEvtStart">De</label>
        <input type="time" id="newEvtStart"/>
        <label for="newEvtEnd">à</label>
        <input type="time" id="newEvtEnd"/>
        <label for="description">Description:</label>
        <textarea id="description"></textarea>
        <button id="submitNewEvt">Valider</button>
        <span id="errNewEvt"></span>
    </div>
    <div class="download">
        <label for="csvStart">Début:</label>
        <input type="date" id="csvStart"/>
        <label for="csvEnd">Fin:</label>
        <input type="date" id="csvEnd"/>
        <button id="csvDownload">Importer</button>
        <span id="errCsv">Les date ne sont pas conformes</span>
    </div>
</div>



