<?php
/* 
 * Fonction pour créer certain input de formulaire
 */

function selectEvent($name){
    // Créer un select du type d'evenement
    global $wpdb;
    $res = "";
    $query = "SELECT * FROM {$wpdb->prefix}event_type";
    $result = $wpdb->get_results($query);
    ob_start();
    echo '<select id="'.$name.'">';
    foreach ($result as $line){
        if($line->type === 'pont-1'){
            echo '<option value="'.$line->id.'">Pont bleu</option>';
        }elseif ($line->type === 'pont-2') {
            echo '<option value="'.$line->id.'">Pont rouge</option>';
        }elseif($line->type !== 'evt'){
            echo '<option value="'.$line->id.'">'.$line->type.'</option>';
        }
    }
    echo '</select>';
    $res .= ob_get_contents();
    ob_end_clean();
    return $res;
}

function selectTime($name,$timeStart,$timeEnd){
    // Créer un select pour les heures
    $ext = ':00';
    ob_start();
    echo "<select id='$name'>";
    while($timeStart !== $timeEnd){
        echo "<option value='$timeStart$ext'>$timeStart$ext</option>";
        if($ext === ':00'){
            $ext = ':30';
        }else{
            $ext = ':00';
            $timeStart++;
        }
    }
    echo "<option value='$timeEnd$ext'>$timeEnd$ext</option>";
    echo "</select>";
    $select = ob_get_contents();
    ob_end_clean();
    return $select;
}