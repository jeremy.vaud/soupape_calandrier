<?php
/* 
 * Fonction de création de calendrier
 */

function generateCalendar($timeStart,$timeEnd,$tabLine){
    // Créer un calandrier d'une semaine
    $tabDay = ["Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche"];
    $tabMonth = ["01" => "Janvier","02" => "Février","03" => "Mars","04" => "Avril","05" => "Mai","06" => "Juin","07" => "Juillet","08" => "Août","09" => "Septembre","10" => "Octobre","11" => "Novembre","12" => "Décembre"];
    ob_start();    
    $date = new DateTime(date("Y-m-d", strtotime('monday this week')));
    echo '<input type="hidden" id="firstDay" value="'.$date->format('F d, Y').'"/>';
    $oneDay = new DateInterval('P1D');
    foreach($tabDay as $day){
        echo "<h2 id='$day'>$day ".(int)$date->format('d')." ".$tabMonth[$date->format('m')]." ".$date->format('Y')."</h2>";
        echo '<button class="btnPeople" data-date="'.$date->format('Y-m-d').'"><i class="fas fa-eye"></i> <span>0</span> adhérents présent</button><div class="people"></div>';
        createCalendar($timeStart,$timeEnd,$day,$tabLine);
        echo "<div class='evtDay' id='".$date->format('D')."'></div></br>";
        $date->add($oneDay);
    }
    $calendar = ob_get_contents();
    ob_end_clean();
    return $calendar;
}

function createCalendar($timeStart,$timeEnd,$day,$tabLine){
    // Créer calandrier d'une journée
    $time = $timeStart;
    echo '<table><thead><tr><th colspan="5"></th>';
    while($time < $timeEnd){
        echo "<th>".$time."H</th><th></th><th></th><th></th>";
        $time += 2;       
    }
    echo '</tr>';
    foreach($tabLine as $title => $id){
        createLine($title,$id,$day,$timeStart,$timeEnd);
    }  
    echo '</table>';
}

function createLine($title,$id,$day,$timeStart,$timeEnd){
    // Créer une ligne du calendrier
     $time = $timeStart;
     echo '<tr><th colspan="5">'.$title.'</th>';
     while($time < $timeEnd){
        $timePlus30 = $time ."-30";
        $timePlus60 = $time+1;
        $timePlus90 = $time+1 ."-30";
        echo "<td class='calendarCase' id='$id"."_"."$day"."_"."$time'></td><td class='calendarCase' id='$id"."_"."$day"."_"."$timePlus30'></td><td class='calendarCase' id='$id"."_"."$day"."_"."$timePlus60'></td><td class='calendarCase' id='$id"."_"."$day"."_"."$timePlus90'></td>";
        $time += 2;
    }
    echo '</tr>';
}

function generateMyCalendar($idUser){
    // Créer une liste des horaires de l'utilisateur
    global $wpdb;
    $tabDay = [1=>"Lundi",2=>"Mardi",3=>"Mercredi",4=>"Jeudi",5=>"Vendredi",6=>"Samedi",7=>"Dimanche"];
    $tabMonth = ["01" => "Janvier","02" => "Février","03" => "Mars","04" => "Avril","05" => "Mai","06" => "Juin","07" => "Juillet","08" => "Août","09" => "Septembre","10" => "Octobre","11" => "Novembre","12" => "Décembre"];
    $currentDate = new DateTime();
    $date = $currentDate->format('Y-m-d');
    $query = "SELECT {$wpdb->prefix}calendar_event.`id`,{$wpdb->prefix}calendar_event.`date`,{$wpdb->prefix}calendar_event.`start_time`,{$wpdb->prefix}calendar_event.`end_time`,{$wpdb->prefix}event_type.`type` AS `event_type` FROM {$wpdb->prefix}calendar_event LEFT JOIN {$wpdb->prefix}event_type ON {$wpdb->prefix}calendar_event.`type_event` = {$wpdb->prefix}event_type.`id` WHERE {$wpdb->prefix}calendar_event.`date` >= '$date' AND {$wpdb->prefix}calendar_event.`user` = '$idUser' ORDER BY {$wpdb->prefix}calendar_event.`date` ASC,{$wpdb->prefix}calendar_event.`start_time` ASC";
    $result = $wpdb->get_results($query);
    $day = ""; 
    $template = "";
    ob_start();
    foreach ($result as $line){
        if($day !== $line->date){
            $day = $line->date;
            $newDate = new DateTime($day);
            echo "<h5>".$tabDay[$newDate->format('N')]." ".$newDate->format('d')." ".$tabMonth[$newDate->format('m')]." ".$newDate->format('Y')."</h5>";
        }   
        echo '<div>';
        echo "<button class='modifTime' data-id='".$line->id."' data-date='".$line->date."' data-start='".$line->start_time."' data-end='".$line->end_time."'><i class='fas fa-pencil-alt'></i> Modifier</button>";
        echo "<button class='supprTime' data-id='".$line->id."' data-date='".$line->date."' data-start='".$line->start_time."' data-end='".$line->end_time."'><i class='fas fa-trash-alt'></i></i> Suprimer</button>";
        echo '<span>De '.$line->start_time.' à '.$line->end_time.': '.$line->event_type.'</span>';
        echo '</div>';
    }
    $template .= ob_get_contents();
    ob_end_clean();
    return $template;
}

function eventList(){
    // Créer la liste des évenement à venir
    global $wpdb;
    $currentDate = new DateTime();
    $date = $currentDate->format('Y-m-d');
    $query = "SELECT * FROM {$wpdb->prefix}soupape_evt WHERE `date` >= '$date' ORDER BY `date`";
    $list = $wpdb->get_results($query);
    $template = "";
    ob_start();
    foreach($list as $evt){
        $explodeDate = explode('-',$evt->date);
        $date = $explodeDate[2]."/".$explodeDate[1]."/".$explodeDate[0];
        $start_time = substr($evt->start_time,0,-3);
        $end_time = substr($evt->end_time,0,-3);
        echo '<div>';
        echo "<h3>".$evt->title."</h3>";
        echo "<p>Le $date de $start_time à $end_time</p>";
        echo "<p>".$evt->description."</p>";
        echo "</div>";
    }
    $template = ob_get_contents();
    ob_end_clean();
    if($template === ""){
        $template = "<p>Aucun évenement de programmé</p>";
    }
    return $template;
}

function generateMonth(){
    // Crée le calendrier du mois
    $template = "";
    ob_start();
    echo'<div id="monthCalendar">';
    $cpt = 0;
    while($cpt !== 30){
        echo'<div id=day-'.$cpt.' class="monthDay" data-date=""><h4></h4><ul></ul></div>';
        $cpt++;
    }
    echo'</div>';
    $template = ob_get_contents();
    ob_end_clean();
    return $template;
}