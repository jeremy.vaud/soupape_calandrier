<?php

/* 
 * Fonction de création du template administrateur
 */
include 'createCalendar.php';

function listUser(){
    // Lister tous les utilisateur
    global $wpdb;
    $query = "SELECT {$wpdb->prefix}users.`ID` AS `user_id`,{$wpdb->prefix}users.`display_name` AS `user_name` FROM {$wpdb->prefix}users";
    return $wpdb->get_results($query);
}

function adminTemplate($timeStart,$timeEnd){
    // Création du template administrateur
    $list = listUser();
    $listUser = "";
    $admin_type = selectEvent('adminAddType');
    $selectStartAdmin = selectTime('adminAddStart', $timeStart, $timeEnd);
    $selectEndAdmin = selectTime('adminAddEnd', $timeStart, $timeEnd);
    $modifStartAdmin = selectTime('modifStart', $timeStart, $timeEnd);
    $modifEndAdmin = selectTime('modifEnd', $timeStart, $timeEnd);
    foreach ($list as $user){
        $listUser .= "<option value='".$user->user_id."'>".$user->user_name."</option>";
    }
    $listValid = listValidation();
    $listValidation = $listValid['html'];
    $nbrValidation = $listValid['nbr'];
    ob_start();
    include 'calendar/template/adminTool.php';
    $template = ob_get_contents();
    ob_end_clean();
    return $template;
}

function listValidation(){
    // Lister les modification en attente de validation et les compter
    global $wpdb;
    $html = "<h5>En attente de validation</h5>";
    $nbr = 0;
    $idAdmin = get_current_user_id();
    $query = "SELECT {$wpdb->prefix}calendar_modif.*,{$wpdb->prefix}calendar_event.`date`,{$wpdb->prefix}calendar_event.`start_time` AS `old_start`,{$wpdb->prefix}calendar_event.`end_time` AS `old_end`,{$wpdb->prefix}users.`display_name` FROM {$wpdb->prefix}calendar_modif LEFT JOIN {$wpdb->prefix}calendar_event ON {$wpdb->prefix}calendar_event.`id` = {$wpdb->prefix}calendar_modif.`id_event` LEFT JOIN {$wpdb->prefix}users ON {$wpdb->prefix}users.`ID` = {$wpdb->prefix}calendar_event.`user`";
    $result = $wpdb->get_results($query);    
    foreach ($result as $line){
        $nbr++;
        $splitDate = explode('-',$line->date);
        $date = $splitDate[2].'/'.$splitDate[1].'/'.$splitDate[0];
        $html .= '<div data-idValid="'.$line->id.'">';
        if($idAdmin === (int)$line->admin){
            $html .= '<button disabled="disabled">Valider</button><button data-id="'.$line->id.'" class="annulation">Annuler</button>';
        }else{
            $html .= '<button data-modif="'.$line->id.'" class="validation" data-evt="'.$line->id_event.'" >Valider</button><button data-id="'.$line->id.'" class="annulation">Annuler</button>';
        }
        if($line->type === 'suppr'){
            $html .= '<span><b>Suppression: </b>'.$line->display_name.' le '.$date.' de '.substr($line->old_start,0,-3).' à '.substr($line->old_end,0,-3).'</span>';
        }elseif($line->type === 'modif'){
            $html .= '<span><b>Modification: </b>'.$line->display_name.' le '.$date.' de '.substr($line->start_time,0,-3).' ('.substr($line->old_start,0,-3).') à '.substr($line->end_time,0,-3).' ('.substr($line->old_end,0,-3).')</span>';
        } 
        $html .= '</div>';
    }
    if($nbr === 0){
        $html .= '<p>Aucune modification en attente de validation</p>';
    }
    return ['html'=>$html,'nbr'=>$nbr];
}