<?php
/* 
 * Ajout js pour ajax
 */

function ajax_enqueue() {
	// Insetion javascript pour l'ajax
	wp_enqueue_script(
		'ajax-script',
		'/calendar/js/calendar.js',
		array('jquery')
	);
	wp_localize_script(
		'ajax-script',
		'ajax_obj',
		array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) )
	);
}
function admin_enqueue() {
	// Insetion javascript administrateur
	wp_enqueue_script(
		'admin-script',
		'/calendar/js/adminJs.js',
		array('jquery')
	);
	wp_localize_script(
		'admin-script',
		'ajax_obj',
		array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) )
	);
}
add_action( 'wp_enqueue_scripts', 'ajax_enqueue' );
add_action( 'wp_enqueue_scripts', 'admin_enqueue' );