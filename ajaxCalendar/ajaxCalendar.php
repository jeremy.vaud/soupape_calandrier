<?php
/**
 * Plugin Name: ajaxCalendar
 * Description: Fontions ajax du calendrier de l'asso
 * Version: 1.0.0
 * Author: Jeremy Vaud
 */

function dataCalendar() {
    // Charger et envoyé tous les données sur une semaine
    if ( isset($_REQUEST) ) {
        global $wpdb;
        $week = intval($_REQUEST['week']);
        $date = new DateTime(date("Y-m-d", strtotime('monday this week')));
        $dateInterval = new DateInterval('P6D');
        if($week !== 0){
            $int = 7*abs($week);
            $weekInterval = new DateInterval('P'.$int.'D');
            if($week > 0){
                $date->add($weekInterval);
            }else{
                $date->sub($weekInterval);
            }
        }
        $startDate = $date->format('Y-m-d');
        $endDate = $date->add($dateInterval)->format('Y-m-d');
        $query = "SELECT {$wpdb->prefix}calendar_event.`id`,{$wpdb->prefix}calendar_event.`date`,{$wpdb->prefix}calendar_event.`start_time`,{$wpdb->prefix}calendar_event.`end_time`,{$wpdb->prefix}users.`ID` AS `user_id`,{$wpdb->prefix}users.`display_name` AS `user_name`,{$wpdb->prefix}event_type.`type` AS `event_type` FROM {$wpdb->prefix}calendar_event LEFT JOIN {$wpdb->prefix}users ON {$wpdb->prefix}calendar_event.`user` = {$wpdb->prefix}users.`ID` LEFT JOIN {$wpdb->prefix}event_type ON {$wpdb->prefix}calendar_event.`type_event` = {$wpdb->prefix}event_type.`id` WHERE {$wpdb->prefix}calendar_event.`date` >= '$startDate' AND {$wpdb->prefix}calendar_event.`date` <= '$endDate'"; 
        $result = $wpdb->get_results($query);
        $peopleByDay = peopleByDay($result);
        $evtList = evtList($startDate, $endDate);
        $data = [$result,$peopleByDay,$evtList];
        header('Content-Type: application/json');
        echo json_encode($data);   
    }   
   die();
}

function evtList($start,$end){
    // Créer un tableau des évenement sur une semaine
    global $wpdb;
    $query = "SELECT * FROM {$wpdb->prefix}soupape_evt WHERE `date` >= '$start' AND `date` <= '$end'";
    $tab = $wpdb->get_results($query);
    $result = [];
    foreach ($tab as $line){
        $date = new DateTime($line->date);
        if(!isset($result[$date->format('D')])){
            $result[$date->format('D')] = [];
        }
        $result[$date->format('D')][] = $line;
    }
    return $result;
}

function insertCalendar(){
    // Verification des donnée puis insertion si valide
    global $wpdb;
    $data = ['success'=>false,'data'=>"Une erreur est survenu"];
    $event_type = $_REQUEST['event_type'];
    $user = $_REQUEST['user'];
    $date = $_REQUEST['date'];
    $start_time = $_REQUEST['start_time'].':00';
    $end_time = $_REQUEST['end_time'].':00';
    try {
        // Verif session
        if((int)$user !== get_current_user_id() && !current_user_can('administrator')){
            throw new Exception("Une erreur est survenue");
        }
        // Verif date
        if(!$_REQUEST['admin']){
            $currentDate = new DateTime();
            $requestDate = new DateTime($date.' '.$start_time);
            if($currentDate > $requestDate){
                throw new Exception("Veuillez choisir une date future");
            }
        }
        // Verif disponibilité
        $check = "SELECT COUNT(*) FROM {$wpdb->prefix}calendar_event WHERE {$wpdb->prefix}calendar_event.`date` = '$date' AND ";
        if($event_type === '1' || $event_type === '2' || $event_type === '6'){
            $check .= "{$wpdb->prefix}calendar_event.`type_event` = '$event_type' AND ";
        }else{
            $check .= "{$wpdb->prefix}calendar_event.`user` = '$user' AND ";
        }
        $check .= "(({$wpdb->prefix}calendar_event.`start_time` < '$start_time' AND {$wpdb->prefix}calendar_event.`end_time` > '$start_time')OR({$wpdb->prefix}calendar_event.`start_time` > '$start_time' AND {$wpdb->prefix}calendar_event.`start_time` < '$end_time') OR {$wpdb->prefix}calendar_event.`start_time` = '$start_time')";
        $result = $wpdb->get_var($check);
        if($result !== '0'){
            throw new Exception("Ce crénaux horaire n'est pas disponible");
        }
        // Insertion dans la bdd
        $data['success'] = $wpdb->insert("{$wpdb->prefix}calendar_event",['user'=>$user,'type_event'=>$event_type,'date'=>$date,'start_time'=>$start_time,'end_time'=>$end_time]);       
    } catch (Exception $exc) {
        $data['data'] = $exc->getMessage();
    }
    header('Content-Type: application/json');
    echo json_encode($data);
    die();
}

function supprCalendar(){
    // Suprimer une ligne de la table calendar_event
    global $wpdb;
    $user = $_REQUEST['user'];
    $id = $_REQUEST['id'];
    if((int)$user !== get_current_user_id()){
        echo '0';
        die();
    }
    echo $wpdb->delete($wpdb->prefix.'calendar_event',['id' => $id,'user' => $user]);
    die();
}

function modifCalendar(){
    // Suprimer une ligne de la table calendar_event
    global $wpdb;
    $user = $_REQUEST['user'];
    $id = $_REQUEST['id'];
    $start = $_REQUEST['start_time'];
    $end = $_REQUEST['end_time'];
    if((int)$user !== get_current_user_id()){
        echo '0';
        die();
    }
    echo $wpdb->update($wpdb->prefix.'calendar_event',['start_time' => $start,'end_time' => $end],['id' => $id,'user' => $user]);
    die();
}



function majMyCalendar(){
    // Récupération des données d'un utilisateur pour son calndrier personnel
    include '../calendar/lib/createCalendar.php';
    $template = generateMyCalendar(get_current_user_id());
    echo $template;
    die();
}

function peopleByDay($data){
    // Créer un tableau des personnes présentes par jour
    $peopleByDay = [];
    foreach ($data as $line){
        if(!$peopleByDay[$line->date]){
            $peopleByDay[$line->date] = [];
        }
        if(!$peopleByDay[$line->date][$line->user_id]){
            $peopleByDay[$line->date][$line->user_id] = ['name' => $line->user_name,'time' => [[$line->start_time, $line->end_time,$line->event_type]]];
        }else{
            $peopleByDay[$line->date][$line->user_id]['time'][] = [$line->start_time, $line->end_time,$line->event_type];
        }
    }
    return $peopleByDay;
}

function userDate(){
    // Lister les horraire d'un utilisateur à une date donnée
    global $wpdb;
    $user = $_REQUEST['user'];
    $date = $_REQUEST['date'];
    $query = "SELECT {$wpdb->prefix}calendar_event.`id`,{$wpdb->prefix}calendar_event.`start_time`,{$wpdb->prefix}calendar_event.`start_time`,{$wpdb->prefix}calendar_event.`end_time`,{$wpdb->prefix}calendar_event.`user`,{$wpdb->prefix}calendar_event.`date`,{$wpdb->prefix}event_type.`type` AS `event_type`,{$wpdb->prefix}calendar_modif.`id` AS `modif_id`,{$wpdb->prefix}calendar_modif.`type` AS `modif_type`,{$wpdb->prefix}calendar_modif.`start_time` AS `modif_start`,{$wpdb->prefix}calendar_modif.`end_time` AS `modif_end`,{$wpdb->prefix}calendar_modif.`admin` FROM {$wpdb->prefix}calendar_event LEFT JOIN {$wpdb->prefix}calendar_modif ON {$wpdb->prefix}calendar_event.`id` = {$wpdb->prefix}calendar_modif.`id_event` LEFT JOIN {$wpdb->prefix}event_type ON {$wpdb->prefix}calendar_event.`type_event` = {$wpdb->prefix}event_type.`id` WHERE {$wpdb->prefix}calendar_event.`user` = '$user' AND {$wpdb->prefix}calendar_event.`date` = '$date'";
    $result = $wpdb->get_results($query);
    header('Content-Type: application/json');
    echo json_encode($result);
    die();
}

function insertEvt(){
    // Insérer un nouvel evt à la bdd
    global $wpdb;
    $success = true;
    $request = [];
    foreach ($_REQUEST as $key => $data){
        if($key !== 'action'){
            $request[$key] = $data;
        }
        if($data === ""){
            $success = false;
        }
    }
    if($success){
        $success = $wpdb->insert("{$wpdb->prefix}soupape_evt",$request);
    }
    $html = "";
    if($success){
        $currentDate = new DateTime();
        $date = $currentDate->format('Y-m-d');
        $query = "SELECT * FROM {$wpdb->prefix}soupape_evt WHERE `date` >= '$date' ORDER BY `date`";
        $list = $wpdb->get_results($query);
        foreach($list as $evt){
            $explodeDate = explode('-',$evt->date);
            $date = $explodeDate[2]."/".$explodeDate[1]."/".$explodeDate[0];
            $start_time = substr($evt->start_time,0,-3);
            $end_time = substr($evt->end_time,0,-3);
            $html .= '<div>';
            $html .= "<h3>".$evt->title."</h3>";
            $html .= "<p>Le $date de $start_time à $end_time</p>";
            $html .= "<p>".$evt->description."</p>";
            $html .= "</div>";
        }
    }
    $data = [$success,$html];
    header('Content-Type: application/json');
    echo json_encode($data);
    die();
}
////////////////////////////////////////////////////////////////////////////////
// Données csv à confirmer
function csvDownload(){
    // Select des donnée du calendrier entre deux date et création d'un tableau
    global $wpdb;
    $data = 'test';
    header('Content-Type: application/json');
    echo json_encode($data);
    die();
}
/////////////////////////////////////////////////////////////////////////////////

function supprAdmin(){
    // Enregistrer une demande de supréssion d'un administrateur
    global $wpdb;
    $idEvt = $_REQUEST['idLine'];
    $admin = $_REQUEST['admin'];
    $data = ['error' => true, 'msg' => 'une erreur est surevenue','list' => ""];
    if(current_user_can('administrator')){
        if($wpdb->get_results("SELECT * FROM {$wpdb->prefix}calendar_modif WHERE `id_event` = $idEvt") !== []){
            $data['msg'] = 'La modification est déjà en attente de validation';
        }else{
            if($wpdb->insert("{$wpdb->prefix}calendar_modif",['id_event' => $idEvt,'type' => 'suppr','admin'=>$admin])){
                $data['error'] = false;
                $data['msg'] = 'Votre demande de suppression à été enregistré';
                include '../calendar/lib/adminFunction.php';
                $data['list'] = listValidation();
            }
        }
    }
    header('Content-Type: application/json');
    echo json_encode($data);
    die();
}

function modifAdmin(){
    // Enregistrer une demande de modification
    global $wpdb;
    $idEvt = $_REQUEST['idLine'];
    $admin = $_REQUEST['admin'];
    $start = $_REQUEST['start-time'];
    $end = $_REQUEST['end-time'];
    $data = ['error' => true, 'msg' => 'une erreur est surevenue','list' => ""];
    if(current_user_can('administrator')){
        if($wpdb->get_results("SELECT * FROM {$wpdb->prefix}calendar_modif WHERE `id_event` = $idEvt") !== []){
            $data['msg'] = 'La modification est déjà en attente de validation';     
        }else{
            if($wpdb->insert("{$wpdb->prefix}calendar_modif",['id_event' => $idEvt,'type' => 'modif','admin'=>$admin, 'start_time' => $start,'end_time' => $end])){
                $data['error'] = false;
                $data['msg'] = 'Votre demande de modification à été enregistré';
                include '../calendar/lib/adminFunction.php';
                $data['list'] = listValidation();
            }
        }
    }
    header('Content-Type: application/json');
    echo json_encode($data);
    die();
}

function deleteModif(){
    // Enregistrer une demande de modification
    global $wpdb;
    $id = $_REQUEST['id'];
    $data = ['error' => true, 'msg' => 'une erreur est surevenue','list' => ""];
    if(current_user_can('administrator')){
        if($wpdb->delete("{$wpdb->prefix}calendar_modif",['id' => $id])){
            $data['error'] = false;
            $data['msg'] = 'La modifiction à bien été annulé';
            include '../calendar/lib/adminFunction.php';
            $data['list'] = listValidation();
        }
    }
    header('Content-Type: application/json');
    echo json_encode($data);
    die();
}

function validModif(){
    // Valider la modification d'un administrateur par un autre administrateur
    global $wpdb;
    $idAdmin = $_REQUEST['admin'];
    $idEvt = $_REQUEST['idEvt'];
    $idModif = $_REQUEST['idModif'];
    $data = ['error' => true, 'msg' => 'une erreur est surevenue','list' => ""];
    if(current_user_can('administrator')){
        $select = $wpdb->get_row("SELECT `admin`,`start_time`,`end_time`,`type` FROM {$wpdb->prefix}calendar_modif WHERE `id` = $idModif");
        if(!($select === null || $select->admin === $idAdmin)){
            if($wpdb->delete("{$wpdb->prefix}calendar_modif",['id' => $idModif])){
                if($select->type === 'suppr'){
                    if($wpdb->delete("{$wpdb->prefix}calendar_event",['id' => $idEvt])){
                        $data['error'] = false;
                        $data['msg'] = 'La suppression a été validé';
                    }
                }elseif ($select->type === 'modif') {
                    if($wpdb->update($wpdb->prefix.'calendar_event',['start_time' => $select->start_time,'end_time' => $select->end_time],['id' => $idEvt])){
                        $data['error'] = false;
                        $data['msg'] = 'La modifiction a été validé';
                    }
                }
            }
        }
        if(!$data['error']){
            include '../calendar/lib/adminFunction.php';
            $data['list'] = listValidation();
        }
    }
    header('Content-Type: application/json');
    echo json_encode($data);
    die();
}

function seeMonth(){
    // Lister les présences sur 30 jours
    $tabDay = ["1"=>"Lun","2"=>"Mar","3"=>"Mer","4"=>"Jeu","5"=>"Ven","6"=>"Sam","7"=>"Dim"];
    $tabMonth = ["01" => "Jan","02" => "Fév","03" => "Mar","04" => "Avr","05" => "Mai","06" => "Juin","07" => "Juil","08" => "Août","09" => "Sept","10" => "Oct","11" => "Nov","12" => "Déc"];
    global $wpdb;
    $data = [];
    $date = new DateTime();
    $startDate = $date->format('Y-m-d');
    $cpt = 0;
    $tabDate = [];
    while ($cpt !== 30){
        $tabDate[$date->format('Y-m-d')] = $cpt;
        $data[$cpt] = ["date" => $tabDay[$date->format('N')]." ".$date->format('d')." ".$tabMonth[$date->format('m')],"pres" => [],'attrDate' => $date->format('Y-m-d')];
        $date->add(new DateInterval('P1D'));
        $cpt++;
    }
    $endDate = $date->format('Y-m-d');
    $query = "SELECT {$wpdb->prefix}calendar_event.`date`,{$wpdb->prefix}calendar_event.`user`,{$wpdb->prefix}users.`display_name`,{$wpdb->prefix}calendar_event.`start_time`,{$wpdb->prefix}calendar_event.`end_time` FROM {$wpdb->prefix}calendar_event LEFT JOIN {$wpdb->prefix}users ON {$wpdb->prefix}calendar_event.`user` = {$wpdb->prefix}users.`ID` WHERE {$wpdb->prefix}calendar_event.`date` >= '$startDate' AND {$wpdb->prefix}calendar_event.`date` <= '$endDate'";
    $result = $wpdb->get_results($query);
    foreach($result as $line){
        if(isset($data[$tabDate[$line->date]]['pres'][$line->user])){
            $data[$tabDate[$line->date]]['pres'][$line->user]['time'] .= ", ".substr($line->start_time,0,-3)." à ".substr($line->end_time,0,-3);
        }else{
           $data[$tabDate[$line->date]]['pres'][$line->user] = ['name'=>$line->display_name,'time'=>substr($line->start_time,0,-3)." à ".substr($line->end_time,0,-3)];  
        } 
    }
    header('Content-Type: application/json');
    echo json_encode($data);
    die();
}

add_action( 'wp_ajax_dataCalendar', 'dataCalendar' );
add_action( 'wp_ajax_insertCalendar', 'insertCalendar' );
add_action( 'wp_ajax_majMyCalendar', 'majMyCalendar' );
add_action( 'wp_ajax_supprCalendar', 'supprCalendar' );
add_action( 'wp_ajax_modifCalendar', 'modifCalendar' );
add_action( 'wp_ajax_insertEvt', 'insertEvt' );
add_action( 'wp_ajax_userDate', 'userDate' );
add_action( 'wp_ajax_csvDownload', 'csvDownload' );
add_action( 'wp_ajax_supprAdmin', 'supprAdmin' );
add_action( 'wp_ajax_modifAdmin', 'modifAdmin' );
add_action( 'wp_ajax_deleteModif', 'deleteModif' );
add_action( 'wp_ajax_validModif', 'validModif' );
add_action( 'wp_ajax_seeMonth', 'seeMonth' );